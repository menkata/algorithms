<?php
//here you can add different length of the array
$arr = range(0, 10);
shuffle($arr );
echo "<h3> Im not sorted! </h3>";

foreach($arr as $a)
{
    echo "<br />" . $a;
    echo "<div style=display:inline-block;height:".$a."px;width:30px;background-color:red> </div>";
}

/**
 * Takes bublesort/2 time
 */

$c = 0;
//we take out the largest element of the array and move it here on every
//step, and our array is getting smaller
$tail = [];
//count how much times are needed for the array to sort
$counter = 0;
//while our array is different then null repeat
while(count($arr) != null)
{
    //on first step this will cycle 9 times untill the largest
    //number is last then we move the largest to tail[]
    //on second step we had take out the largest and will cycle 8 times
    //and son on until there is nothing left 
    for($i=0; $i<count($arr)-1; $i++)
    {
        $counter++;
        if($arr[$i] > $arr[$i+1])
        {
            $c = $arr[$i];
            $arr[$i] = $arr[$i+1];
            $arr[$i+1] = $c;
        }
    }
    //take the biggest number 
    $biggest = array_pop($arr);
    //insert it as first element of array in tail[]
    array_unshift($tail,$biggest);
};
echo "<h3> Im sorted! I needed $counter cycle</h3>";
foreach($tail as $a)
{
    echo "<br />" . $a;
    echo "<div style=display:inline-block;height:".$a."px;width:30px;background-color:red> </div>";
}